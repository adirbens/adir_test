# Welcome to Conway's Game of Life!

Inside 'adir_test's folder, you should create another directory named 'build'.
Then, use cd to 'build' folder and write these commands using the terminal:
```
cmake ..
cmake --build .
```
Where these commands will compile our project and create the relevant executable files:
The first one is *game_of_life*, and the second one is *game_of_life_test*.

Now we can run the first executable file, which is our main program:
```
./game_of_life
```
For now, if succeed, the user should see the following messages:

'Please enter the width of the cells's matrix for each process: ' <br />
'Please enter the length of the cells's matrix for each process: ' <br />
'Please enter the number of generations: ' 

The user should type his desired values. <br />
If invalid input is given, should receive a failure message and a chance to try again.

In order to use MPI, when running the executable you should use:
```
mpirun -n N ./game_of_life
```
where *N* is the number of processes you want your program to use. <br />

We can also run the program with the following flags:

### '-t':
-t (terminal) flag determines that the values of the cells matrix within
all generations will be shown on the terminal.

### '-f':
-f (file) flag determines that the values of the cells matrix of
all generations will be written to an output file named 'outputX.txt', located at 'build/output directory, where X will be replaced with the rank of the current process. That is, if 5 processes are running, 5 output files will be created.

### '-r':
-r (random) flag determines that we will generate random values at every run (of the same compilation) - if not being used, 
we use the same initial random values (can be a good use for testing).

### '-c':
-c (cyclic) flag determines that we want to consider our matrix as cyclic.

### '-p':
-p (plot) flag determines that we want to compare and plot the runtime of our program with different number of threads. The data will be written to an output file named 'plotData.txt'. This flag is no longer being used (explained below).

### '-d':
-d (demo) flag determines that we want to use demo calculation of the next generations (we simply perform NOT operation on each cell).

### '-m':
-m (communication) flag determines that we want to measure the time of the communication of each process, and write it to an output file.

<ins>MPI related flags:</ins><br/>
### '-n':
-n (no input) flag determines that we dont want to wait for I/O and we use default values for the topology of the network (can also be changed by a flag - shown below). This flag being used for time measure of our program.

### 'matrixr=X':
This flag determines the row number (the value X) of the matrix (of each process!). We will use this flag when we want to determine the size without user input (when *-n* flag is on).

### 'matrixc=X':
This flag determines the column number (the value X) of the matrix (of each process!). We will use this flag when we want to determine the size without user input (when *-n* flag is on).

### 'gen=X':
This flag determines the number of generations we want to perform. We will use this flag when we want to determine the number without user input (when *-n* flag is on).

### 'row=X':
We are using cartesian topology. This flag detemines the row number of processes (the value X).

### 'col=X':
We are using cartesian topology. This flag detemines the column number of processes (the value X).

### 'reorder=X':
This flag determines if we allow MPI to find a good way to assign the process to the elements of the decomposition (X can be 0 or 1, where the default is 0).

### 'threads=X':
This flag determines the number of threads running on each process. In order to use this flag properly, make sure you turn on the relevant flag/s in CMakeLists.txt (PARALLEL_OUTER and/or PARALLEL_INNER) - you can see the relevant affected code in *game_of_life.cpp* file.

<ins>VISIT related flags:</ins><br/>
### '-b':
-b (batch) flag determines that we want to run our simulation with visit application under batch mode.

### '-i':
-i (interactive) flag determines that we want to run our simulation with visit application under interactive mode.

For example:
```
mpirun -n 12 ./game_of_life -n row=4 col=3 matrixr=1000 matrixc=1000 gen=50 threads=2 -c
```
The command ebove will run a program where we will not wait for user input, there will be 12 processes (4 rows x 3 columns) where each one has 2 threads. The matrix size of each process will be 1000x1000, and all processes will perform 50 generations. Also, the complete matrix will be considered cyclic.

**_NOTE:_** The order of the flag does not matter. 
Also, make sure that the number of the processes is equal to row x col exactly, otherwise you will get an error.


We can also run the second executable file, which is our test file:
```
./game_of_life_test
```
**_NOTE:_** For now, not all of the tests are passing, changes need to be made in the context of MPI for them to pass.

### Additional files:

<ins>game_of_life_plot.py</ins><br/>
This file was being used in order to show graphically results of the runtime as a function of number of threads (after using the *-p* flag - explained above). This is no longer used since the addition of processes (MPI), and relevant alternatives are presented below.

<ins>game_of_life_data.py</ins><br/>
When running *game_of_life* with the -f flag, as explained above, output file will be created for each process, presenting his matrix over the generations. However, each process knows only part of the whole matrix, therefore we want the option to see the "big picture". running this script will collect the data from the partial matrices and create a new file named *output.txt*, which contains the data of the complete matrix over the generations. Using the flag *-t* will also present the output to the terminal (with colors).

<ins>game_of_life_times.sh</ins><br/>
This script helps us examine the differnt runtimes depending on different parameters such as: matrix size, process number, thread number and so on. The values can be changed easily in the script. successful run will create a new file named *times.txt*, presenting the different times and configurations.

<ins>game_of_life_scale.py</ins><br/>
This script uses *times.txt* file presented above, and creates a png file of a graph with the relevant values, according to the different times and configurations found in the file.

<ins>game_of_life_communication.py</ins><br/>
This script provides 3 services (that are being used by the next file) related to communication part between the processes. Additional information can be found in the file.

<ins>game_of_life_communication.sh</ins><br/>
This script helps us examine the differnt runtimes and communication runtime depending on different parameters such as: matrix size, process number, thread number and so on. The values can be changed easily in the script. This file uses the script above, can be easily changed according to the desired service we want to use.

**_NOTE:_**  Whenever we run the program with multipule threads/processes, we should make sure we turn off 'USE_PROFILE' flag in CMakeLists.txt. We want to turn this flag on only when we use single thread AND single process (probably not relevant anymore), in order to use 'gprof' and examine the performance of our program in general and our different functions in particular (anyway, we will probably not be using this option anymore - it was helpful to examine the performance before using MPI/OpenMP).
<br/>In addition, make sure that you always change/use the values of the relevant flags in CMakeLists according to the current goal of the run. Even if you changed and compiled with the desired values and flags, I recommend to check *CMakeLists.cache* and make sure that old values are no longer.
