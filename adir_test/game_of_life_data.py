from os.path import exists
from colorama import Fore, Back, Style
import sys


DATA_DIR_PATH = r"build/output/"



def findConf(dataFile):
    rawData = dataFile.readline()
    processes = rawData[rawData.index("processes:") + len("processes:"):].strip().split("x")
    matrix = rawData[rawData.index("matrix:") + len("matrix:"): rawData.index("processes:") - 2].strip().split("x")
    gen = rawData[rawData.index("Generation number:") + len("Generation number:"): rawData.index("rank:") - 2].strip().split("/")
    return int(processes[0]), int(processes[1]), int(matrix[0]), int(gen[1])


def readGenMatrix(rank, gen, rowsEach):
    try:
        with open(DATA_DIR_PATH + "output" + str(rank) +".txt", 'r') as dataFile:
            data = dataFile.read()
            relevantIndex = data.index("Generation number:" + str(gen))
            relevantData = data[relevantIndex:].splitlines()[1:rowsEach + 1]
    except Exception as e:
        print("\n### An error has occurred while trying to read the file ###\n")
        exit(1)
    return relevantData


def writeMatrix(i, j, rowsEach, gen, rank, data):
    matrix = readGenMatrix(rank, gen, rowsEach)
    for index in range(rowsEach):
        data[gen][i * rowsEach + index] += matrix[index]


def writeToFile(data, dataFile):
    for genNum in range(len(data)):
        dataFile.write("Generation number: " + str(genNum) + "\n")
        for line in data[genNum]:
            dataFile.write(line + "\n")
        dataFile.write("\n")


def writeToTerminal(dataFile):
    for line in dataFile:
        if "Gen" in line or line == "":
            print(line)
        else:
            for cell in line:
                if cell == "X":
                    print(Back.RED + "X", end="")
                elif cell == "O":
                    print(Back.GREEN + "O", end="")
                else:
                    print(cell, end="")
                print(Style.RESET_ALL, end="")
            print()



def main():

    if not exists(DATA_DIR_PATH):
        print("\n### Data directory is not ready yet or mislocated ###\n")
        exit(1)

    try:
        with open(DATA_DIR_PATH + "output0.txt", 'r') as dataFile0:
            processRows, processCols, rowsEach, maxGenNum = findConf(dataFile0)
    except Exception as e:
        print("\n### An error has occurred while trying to read the first file ###\n")
        exit(1)


    data = [["" for x in range(processRows * rowsEach)] for y in range(maxGenNum + 1)]
    for gen in range(maxGenNum + 1):
        for i in range(processRows):
            for j in range(processCols):
                writeMatrix(i, j, rowsEach, gen, i * processCols + j, data)

    
    try:
        with open(DATA_DIR_PATH + "output.txt", 'w') as dataFile:
            writeToFile(data, dataFile)
    except Exception:
        print("\n### An error has occurred while trying to write to output file ###\n")
        exit(1)
    
    if "-t" in sys.argv:
        try:
            with open(DATA_DIR_PATH + "output.txt", 'r') as dataFile:
                writeToTerminal(dataFile)
        except Exception:
            print("\n### An error has occurred while trying to read output file ###\n")
            exit(1)

    
if __name__ == "__main__":
    main()