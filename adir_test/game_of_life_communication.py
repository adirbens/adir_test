import matplotlib.pyplot as plt
from os.path import exists
import os
import sys


DATA_DIR_PATH = r"build/communicationTimes/"
OUTPUT_DIR_PATH = r"communicationTimes/"


def writeTimes(processNum):
    if not exists(OUTPUT_DIR_PATH):
        print("\n### Data directory is not ready yet or mislocated ###\n")
        exit(1)
    try:
        for i in range(processNum):
            fileName = f"output{i}.txt"
            filePath = OUTPUT_DIR_PATH + fileName
            with open(filePath, 'r') as dataFile:
                #times = dataFile.read().strip().split()
                #print(times)
                totalTime = sum([round(float(time), 3) for time in dataFile.read().strip().split()])   
            #os.makedirs(os.path.dirname(filePath), exist_ok=True)
            with open(filePath, "w") as f:
                f.write(str(totalTime))
    except Exception as e:
        print("\n### An error has occurred ###\n")
        exit(1)


def getTimes(processNum):
    try:
        times = []
        for i in range(processNum):
            fileName = f"output{i}.txt"
            filePath = OUTPUT_DIR_PATH + fileName
            with open(filePath, 'r') as dataFile:
                times.append(float(dataFile.read().strip()))
    except Exception as e:
        print("\n### An error has occurred ###\n")
        exit(1)
    return times


def plotLoadBalance(processNum, isCyclic, totalTime, matrixSize, threadNum):
    if not exists(OUTPUT_DIR_PATH):
        print("\n### output directory is not ready yet or mislocated ###\n")
        exit(1)
    times = [time / float(totalTime) for time in getTimes(processNum)]     
    plt.bar(range(processNum), times)
    plt.title(f"Total runtime: {totalTime}, Thread per process: {threadNum}")
    plt.xlabel("Process rank")
    plt.ylabel("Communication time / Total runtime")
    plt.savefig(OUTPUT_DIR_PATH +f"loadBalance{processNum}process{isCyclic}cyclic{matrixSize}matrix.png")
    print("TOTAL RUNTIME: ", totalTime)
    
    
def calcAvgMaxRatio(processNum):
    if not exists(OUTPUT_DIR_PATH):
        print("\n### output directory is not ready yet or mislocated ###\n")
        exit(1)
    times = getTimes(processNum)
    print("AVG/MAX: ", (sum(times) / len(times)) / max(times) )



def main():
    pass

    
if __name__ == "__main__":
    main()