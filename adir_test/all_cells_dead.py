from pathlib import Path
import os
from visit import *
from time import sleep


paths = sorted(Path("/home/adirbens/.visit/simulations/").iterdir(), key=os.path.getmtime)
simfile = str(paths[-1])
step = 1
maxStep = 400
sumOld = -1
sumNew = -2
sumNew2 = -3


Launch()
OpenDatabase(simfile)
sleep(5)

AddPlot("Mesh", "cells_mesh")
AddPlot("Pseudocolor", "cells")
DrawPlots()
SetActivePlots(1)


while step < maxStep and not (sumOld == sumNew == sumNew2) and sumNew2 != 0:
    SendSimulationCommand("rfwwwlhpchim1", simfile, "step", "clicked();step;QPushButton;Simulations;NONE")
    if step % 10 == 0:
        SendSimulationCommand("rfwwwlhpchim1", simfile, "update", "clicked();update;QPushButton;Simulations;NONE")
        sleep(1)
        Query("Variable Sum")
        sumOld = sumNew
        sumNew = sumNew2
        sumNew2 = GetQueryOutputValue()
        print(sumNew2)
    step += 1
    
SendSimulationCommand("rfwwwlhpchim1", simfile, "quit", "clicked();quit;QPushButton;Simulations;NONE")

sleep(5)