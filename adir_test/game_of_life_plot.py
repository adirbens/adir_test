import matplotlib.pyplot as plt
from os.path import exists


DATA_FILE_PATH = r"build/plotData.txt"


def readDataFile(dataFile):
    allData = dataFile.read().split('\n')
    matrixInfo = tuple(data for data in allData[0].split('x'))
    threadTimesInfo = [tuple([float(value) for value in data.split(',')]) for data in allData[1:]]
    return matrixInfo, threadTimesInfo


def getParallelLevel(matrixInfo):
    l = len(matrixInfo)
    if l == 0:
        return "no_parallel"
    elif l == 1:
        return matrixInfo[0] + "_loop"
    else:
        return matrixInfo[0] + "_&_" + matrixInfo[1] + "_loop"


def plotDataFile(matrixInfo, threadTimesInfo):
    parallel = getParallelLevel(matrixInfo[3:])
    plotTitle = f"size: {matrixInfo[0]}x{matrixInfo[1]}, generations: {matrixInfo[2]}, parallel: {parallel}"
    plt.title(plotTitle)
    plt.plot([1 / data[0] for data in threadTimesInfo], [data[1] for data in threadTimesInfo])
    plt.plot([1 / data[0] for data in threadTimesInfo], [data[1] for data in threadTimesInfo], 'or', markersize=1)
    plt.xlabel("1 / Number of threads")
    plt.ylabel("runtime in seconds")
    plt.savefig(f"{parallel}.png")



def main():

    if not exists(DATA_FILE_PATH):
        print("\n### Data file is not ready yet or mislocated ###\n")
        exit(1)

    try:
        with open(DATA_FILE_PATH, 'r') as datafile:
            matrixInfo, threadTimesInfo = readDataFile(datafile)
    except Exception:
        print("\n### An error has occurred while trying to read the file ###\n")
        exit(1)

    plotDataFile(matrixInfo, threadTimesInfo)


if __name__ == "__main__":
    main()