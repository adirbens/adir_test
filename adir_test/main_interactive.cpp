#include <iostream>
#include <game_of_life.h>
#include <mpi.h>
#include <VisItControlInterface_V2.h>
#include <VisItDataInterface_V2.h>
#include "SimulationExample.h"
#include <string.h>
using namespace std;



#define SIM_STOPPED       0
#define SIM_RUNNING       1

#define VISIT_COMMAND_PROCESS 0
#define VISIT_COMMAND_SUCCESS 1
#define VISIT_COMMAND_FAILURE 2


static int visit_broadcast_int_callback(int *value, int sender, void *cbdata)
{
    return MPI_Bcast(value, 1, MPI_INT, sender, MPI_COMM_WORLD);
}

static int visit_broadcast_string_callback(char *str, int len, int sender, void *cbdata)
{
    // simulation_data *sim = (simulation_data *)cbdata;
    return MPI_Bcast(str, len, MPI_CHAR, sender, MPI_COMM_WORLD);
}






visit_handle SimGetMetaData(void *cbdata)
{
    visit_handle md = VISIT_INVALID_HANDLE;
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;

    /* Create metadata. */
    if(VisIt_SimulationMetaData_alloc(&md) == VISIT_OKAY)
    {
        visit_handle mmd = VISIT_INVALID_HANDLE;
        visit_handle vmd = VISIT_INVALID_HANDLE;

        /* Set the simulation state. */
        VisIt_SimulationMetaData_setMode(md, (cellMatrix->getRunMode() == SIM_STOPPED) ?
            VISIT_SIMMODE_STOPPED : VISIT_SIMMODE_RUNNING);
        VisIt_SimulationMetaData_setCycleTime(md, cellMatrix->getCycle(), cellMatrix->getTime());

        /* Fill in the mesh metadata. */
        if(VisIt_MeshMetaData_alloc(&mmd) == VISIT_OKAY)
        {
            /* Set the mesh's properties.*/
            VisIt_MeshMetaData_setName(mmd, "cells_mesh");
            VisIt_MeshMetaData_setMeshType(mmd, VISIT_MESHTYPE_RECTILINEAR);
            VisIt_MeshMetaData_setTopologicalDimension(mmd, 2);
            VisIt_MeshMetaData_setSpatialDimension(mmd, 2);

            int ndoms = cellMatrix->getSize();  // ?????
            VisIt_MeshMetaData_setNumDomains(mmd, ndoms);
            VisIt_MeshMetaData_setDomainTitle(mmd, "patches");
            VisIt_MeshMetaData_setDomainPieceName(mmd, "patch");

            VisIt_SimulationMetaData_addMesh(md, mmd);
        }

        /* Add a variable. */
        if(VisIt_VariableMetaData_alloc(&vmd) == VISIT_OKAY)
        {
            VisIt_VariableMetaData_setName(vmd, "cells");
            VisIt_VariableMetaData_setMeshName(vmd, "cells_mesh");
            VisIt_VariableMetaData_setType(vmd, VISIT_VARTYPE_SCALAR);
            VisIt_VariableMetaData_setCentering(vmd, VISIT_VARCENTERING_ZONE);

            VisIt_SimulationMetaData_addVariable(md, vmd);
        }
        
        /* Add some custom commands. */
        const char *cmd_names[] = {"halt", "step", "run", "update", "save png", "save visit", "reset", "quit"};
        for(size_t i = 0; i < sizeof(cmd_names)/sizeof(const char *); i++)
        {
            visit_handle cmd = VISIT_INVALID_HANDLE;
            if(VisIt_CommandMetaData_alloc(&cmd) == VISIT_OKAY)
            {
                VisIt_CommandMetaData_setName(cmd, cmd_names[i]);
                VisIt_SimulationMetaData_addGenericCommand(md, cmd);
            }
        }
        
        
    }
    return md;
}

visit_handle SimGetMesh(int domain, const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;

    /* Get the patch with the appropriate domain id. */
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;

    if(strcmp(name, "cells_mesh") == 0 && cellMatrix->getRank() == domain)
    {
        int i;

        if(VisIt_RectilinearMesh_alloc(&h) != VISIT_ERROR)
        {
            /* Initialize X coords. */
            float *coordX = (float *)malloc(sizeof(float) * (cellMatrix->getLength() + 1));
            int xOffset = (cellMatrix->getRank() % cellMatrix->getMeshCol()) * cellMatrix->getLength();
            for(i = 0; i < cellMatrix->getLength() + 1; i++)
            {
                coordX[i] = i + xOffset;
            }
            /* Initialize Y coords. */
            float *coordY = (float *)malloc(sizeof(float) * (cellMatrix->getWidth() + 1));
            int yOffset = (cellMatrix->getRank() / cellMatrix->getMeshRow()) * cellMatrix->getWidth();
            for(i = 0; i < cellMatrix->getWidth() + 1; i++)
            {
                coordY[i] = i + yOffset;
            }

            /* Give the mesh some coordinates it can use. */
            visit_handle xc, yc;
            VisIt_VariableData_alloc(&xc);
            VisIt_VariableData_alloc(&yc);
            if(xc != VISIT_INVALID_HANDLE && yc != VISIT_INVALID_HANDLE)
            {
                VisIt_VariableData_setDataF(xc, VISIT_OWNER_VISIT, 1, cellMatrix->getLength() + 1, coordX);
                VisIt_VariableData_setDataF(yc, VISIT_OWNER_VISIT, 1, cellMatrix->getWidth() + 1, coordY);
                VisIt_RectilinearMesh_setCoordsXY(h, xc, yc);
            }
            else
            {
                free(coordX);
                free(coordY);
            }
        }
    }

    return h;
}

int* getMatrixNoGhosts(CellMatrix* cellMatrix){
    int* matrixNoGhosts = (int*)malloc(sizeof(int) * cellMatrix->getLength() * cellMatrix->getWidth());
    for(int i = 0; i < cellMatrix->getWidth(); i++){
        for(int j = 0; j < cellMatrix->getLength(); j++){
            matrixNoGhosts[i * cellMatrix->getWidth() + j] = cellMatrix->getCell(i + 1, j + 1);
        }
    }
    return matrixNoGhosts;
}

visit_handle SimGetVariable(int domain, const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;

    /* Get the patch with the appropriate domain id. */
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;

    if(strcmp(name, "cells") == 0 && cellMatrix->getRank() == domain)
    { 
        VisIt_VariableData_alloc(&h);
        VisIt_VariableData_setDataI(h, VISIT_OWNER_SIM, 1,
            cellMatrix->getLength() * cellMatrix->getWidth(), getMatrixNoGhosts(cellMatrix));
    }

    return h;
}

visit_handle SimGetDomainList(const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;
    if(VisIt_DomainList_alloc(&h) != VISIT_ERROR)
    {
        visit_handle hdl = VISIT_INVALID_HANDLE;
        CellMatrix *cellMatrix = (CellMatrix *)cbdata;
        //int npatches = patch_num_patches(&sim->patch);
        
        int * rank = (int*)malloc(sizeof(int));
        rank[0] = cellMatrix->getRank();
        //if(sim->npatch_list > 0)
        //{
        VisIt_VariableData_alloc(&hdl);
        VisIt_VariableData_setDataI(hdl, VISIT_OWNER_SIM, 1, 1, rank);
        //}

        VisIt_DomainList_setDomains(h, cellMatrix->getSize(), hdl);
    }
    return h;
}






void ui_step_clicked(void *cbdata)
{
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;
    printf("ui_step_clicked\n");
    cellMatrix->fullStep();
}
void ui_halt_clicked(void *cbdata)
{
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;
    printf("ui_halt_clicked\n");
    cellMatrix->setRunMode(SIM_STOPPED);
    VisItTimeStepChanged();
}

void ui_run_clicked(void *cbdata)
{
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;
    printf("ui_run_clicked\n");
    cellMatrix->setRunMode(SIM_RUNNING);
    VisItTimeStepChanged();
}

void ui_reset_clicked(void *cbdata)
{
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;
    printf("ui_reset_clicked\n");
    cellMatrix->initilizeFromOrigin();
}



void ControlCommandCallback(const char *cmd, const char *args, void *cbdata)
{
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;

    if(strcmp(cmd, "halt") == 0)
        cellMatrix->setRunMode(SIM_STOPPED);
    else if(strcmp(cmd, "step") == 0)
        cellMatrix->fullStep();
    else if(strcmp(cmd, "run") == 0)
        cellMatrix->setRunMode(SIM_RUNNING);
    else if(strcmp(cmd, "update") == 0)
    {
        VisItTimeStepChanged();
        VisItUpdatePlots();
    }
    else if(strcmp(cmd, "save png") == 0){
        VisItTimeStepChanged();
        // VisItAddPlot("Pseudocolor", "cells");
        // VisItAddPlot("Mesh", "cells_mesh");
        // VisItDrawPlots();
        VisItUpdatePlots();
        char filename[100];
        sprintf(filename, "game_of_life_%06d.png", cellMatrix->getCycle());
        if(VisItSaveWindow(filename, 1000, 1000, VISIT_IMAGEFORMAT_PNG) == VISIT_OKAY)
        {
            if(cellMatrix->getRank() == 0){
                printf("Rendered to %s\n", filename);
            }
        }
        else
        {
            if(cellMatrix->getRank() == 0){
                printf("Failed to save %s\n", filename);
            }
        }
    }
    else if(strcmp(cmd, "save visit") == 0){
        VisItTimeStepChanged();
        
        // add a pseudocolor plot
        // VisItAddPlot("Pseudocolor", "cells");    
        // VisItAddPlot("Mesh", "cells_mesh");
        // VisItDrawPlots();
        VisItUpdatePlots();
        
        // export data to every 10th cycle
        char filename[100];
        visit_handle vars = VISIT_INVALID_HANDLE;
        VisIt_NameList_alloc(&vars);
        VisIt_NameList_addName(vars, "default");
        sprintf(filename, "cells_export_%06d", cellMatrix->getCycle());
        if(VisItExportDatabase(filename, "VTK_1.0", vars) && cellMatrix->getRank() == 0){
            printf("Exported to %s\n", filename);
        }
        VisIt_NameList_free(vars);
    }
    else if(strcmp(cmd, "quit") == 0){
        cellMatrix->setDone(1);
    }
    else if(strcmp(cmd, "reset") == 0)
        cellMatrix->initilizeFromOrigin();
    else
    {
        if(cellMatrix->getRank() == 0)
        {
            fprintf(stderr, "cmd=%s, args=%s\n", cmd, args);
        }
    }
}

/* Helper function for ProcessVisItCommand */
static void BroadcastSlaveCommand(int *command, CellMatrix *sim)
{
    MPI_Bcast(command, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

/* Callback involved in command communication. */
void SlaveProcessCallback(void *cbdata)
{
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;
    int command = VISIT_COMMAND_PROCESS;
    BroadcastSlaveCommand(&command, cellMatrix);
}

/* Process commands from viewer on all processors. */
int ProcessVisItCommand(CellMatrix *cellMatrix)
{
    int command=0;
    if (cellMatrix->getRank() == 0)
    {  
        int success = VisItProcessEngineCommand();

        if (success == VISIT_OKAY)
        {
            command = VISIT_COMMAND_SUCCESS;
            BroadcastSlaveCommand(&command, cellMatrix);
            return 1;
        }
        else
        {
            command = VISIT_COMMAND_FAILURE;
            BroadcastSlaveCommand(&command, cellMatrix);
            return 0;
        }
    }
    else
    {
        /* Note: only through the SlaveProcessCallback callback
         * above can the rank 0 process send a VISIT_COMMAND_PROCESS
         * instruction to the non-rank 0 processes. */
        while (1)
        {
            BroadcastSlaveCommand(&command, cellMatrix);
            switch (command)
            {
            case VISIT_COMMAND_PROCESS:
                VisItProcessEngineCommand();
                break;
            case VISIT_COMMAND_SUCCESS:
                return 1;
            case VISIT_COMMAND_FAILURE:
                return 0;
            }
        }
    }
}

void ProcessConsoleCommand(CellMatrix *cellMatrix)
{
    /* Read A Command */
    char cmd[1000];

    if (cellMatrix->getRank() == 0)
    {
        if(VisItReadConsole(1000, cmd) == VISIT_ERROR)
        {
            sprintf(cmd, "quit");
            printf("quit\n");
        }
    }

    /* Broadcast the command to all processors. */
    MPI_Bcast(cmd, 1000, MPI_CHAR, 0, MPI_COMM_WORLD);

    if(strcmp(cmd, "quit") == 0)
        cellMatrix->setDone(1);
    else if(strcmp(cmd, "halt") == 0)
    {
        cellMatrix->setRunMode(SIM_STOPPED);
        VisItTimeStepChanged();
    }
    else if(strcmp(cmd, "step") == 0)
        cellMatrix->fullStep();
    else if(strcmp(cmd, "run") == 0)
    {
        cellMatrix->setRunMode(SIM_RUNNING);
        VisItTimeStepChanged();
    }
    else if(strcmp(cmd, "save png") == 0){
        VisItTimeStepChanged();
        VisItAddPlot("Pseudocolor", "cells");
        VisItAddPlot("Mesh", "cells_mesh");
        VisItDrawPlots();
        VisItUpdatePlots();
        char filename[100];
        sprintf(filename, "game_of_life_%06d.png", cellMatrix->getCycle());
        if(VisItSaveWindow(filename, 1000, 1000, VISIT_IMAGEFORMAT_PNG) == VISIT_OKAY)
        {
            if(cellMatrix->getRank() == 0){
                printf("Rendered to %s\n", filename);
            }
        }
        else
        {
            if(cellMatrix->getRank() == 0){
                printf("Failed to save %s\n", filename);
            }
        }
    }
    else if(strcmp(cmd, "save visit") == 0){
        VisItTimeStepChanged();
        VisItAddPlot("Pseudocolor", "cells");
        VisItAddPlot("Mesh", "cells_mesh");
        VisItDrawPlots();
        VisItUpdatePlots();
        visit_handle vars = VISIT_INVALID_HANDLE;
        VisIt_NameList_alloc(&vars);
        VisIt_NameList_addName(vars, "default");
        char filename[100];
        sprintf(filename, "cells_export_%06d", cellMatrix->getCycle());
        int handle = VisItExportDatabase(filename, "VTK_1.0", vars);
        if(cellMatrix->getRank() == 0){
            if(handle == VISIT_OKAY){
                printf("Exported to %s\n", filename);
            }
            else{
                printf("Couldn't export visit file\n");
            }
        }
        VisIt_NameList_free(vars);
    }
    else if(strcmp(cmd, "update") == 0)
    {
        if(cellMatrix->getRank() == 0)
             std::cout << "VisItTimeStepChanged() before update" << std::endl;
        VisItTimeStepChanged();
        VisItUpdatePlots();
    }
    else if(strcmp(cmd, "reset") == 0)
    {
        cellMatrix->initilizeFromOrigin();
    }
    else if(strcmp(cmd, "help") == 0 && cellMatrix->getRank() == 0)
    {
        printf("Commands:\n");
        printf("   quit           Quit the simulation\n");
        printf("   halt           Halt the simulation\n");
        printf("   step           Let the simulation run one step\n");
        printf("   run            Let the simulation run\n");
        printf("   save png       Save png image of the current mesh\n");
        printf("   update         Tell VisIt to update the plots with new data\n");
        printf("   reset          Reset the simulation to its initial settings\n");
    }
    
    if(cellMatrix->getRank() == 0)
    {
        fprintf(stderr, "Command %s completed.\n", cmd);
        fflush(stderr);
    }
}



void mainloop_batch(int maxStepNum, CellMatrix* cellMatrix, FlagReader* flagReader){

    int blocking, visitstate = 0, err = 0;
    
    /* Register some ui actions */
    VisItUI_clicked("STEP", ui_step_clicked, cellMatrix);
    VisItUI_clicked("HALT", ui_halt_clicked, cellMatrix);
    VisItUI_clicked("RUN", ui_run_clicked, cellMatrix);
    VisItUI_clicked("RESET", ui_reset_clicked, cellMatrix);
    
    /* If we're not running by default then simulate once there's something
     * once VisIt connects.
     */
    if(cellMatrix->getRunMode() == SIM_STOPPED)
        cellMatrix->fullStep();

    /* main loop */
    if(cellMatrix->getRank() == 0)
    {
        fprintf(stderr, "command> ");
        fflush(stderr);
    }
    
    do
    {
        blocking = (cellMatrix->getRunMode() == SIM_STOPPED) ? 1 : 0;
        /* Get input from VisIt or timeout so the simulation can run. */
        if(cellMatrix->getRank() == 0)
        {
            visitstate = VisItDetectInput(blocking, fileno(stdin));
        }
        /* Broadcast the return value of VisItDetectInput to all procs. */
        MPI_Bcast(&visitstate, 1, MPI_INT, 0, MPI_COMM_WORLD);
        
        /* Do different things depending on the output from VisItDetectInput. */
        switch(visitstate)
        {
        case 0:
            /* There was no input from VisIt, return control to sim. */
            cellMatrix->fullStep();
            break;
        case 1:
            /* VisIt is trying to connect to sim. */
            if(VisItAttemptToCompleteConnection() == VISIT_OKAY)
            {
                if(cellMatrix->getRank() == 0)
                {
                    fprintf(stderr, "VisIt connected\n");
                }
                VisItSetCommandCallback(ControlCommandCallback, (void*)cellMatrix);
                VisItSetSlaveProcessCallback2(SlaveProcessCallback, (void*)cellMatrix);

                VisItSetGetMetaData(SimGetMetaData, (void*)cellMatrix);
                VisItSetGetMesh(SimGetMesh, (void*)cellMatrix);
                VisItSetGetVariable(SimGetVariable, (void*)cellMatrix);
                VisItSetGetDomainList(SimGetDomainList, (void*)cellMatrix);
            }
            else 
            {
                /* Print the error message */
                char *err = VisItGetLastError();
                if(cellMatrix->getRank() == 0)
                {
                    fprintf(stderr, "VisIt did not connect: %s\n", err);
                }
                free(err);
            }
            break;
        case 2:
            /* VisIt wants to tell the engine something. */
            if(!ProcessVisItCommand(cellMatrix))
            {
                /* Disconnect on an error or closed connection. */
                VisItDisconnect();
                /* Start running again if VisIt closes. */
                /*sim->runMode = SIM_RUNNING;*/
            }
            break;
        case 3:
            /* VisItDetectInput detected console input - do something with it.
             * NOTE: you can't get here unless you pass a file descriptor to
             * VisItDetectInput instead of -1.
             */
            ProcessConsoleCommand(cellMatrix);
            if (cellMatrix->getRank() == 0)
            {
                fprintf(stderr, "command> ");
                fflush(stderr);
            }
            break;
        default:
            if(cellMatrix->getRank() == 0)
            {
                fprintf(stderr, "Can't recover from error %d!\n", visitstate);
            }
            err = 1;
            break;
        }
        
    } while(!cellMatrix->getDone() && err == 0);

}





int main(int argc, char *argv[]){

    FlagReader flagReader(0);
    flagReader.initialFlags(argc, argv);

    
    /* Initialize environment and simulation struct. */
    char *env = NULL;
    // simulation_data sim;
    // simulation_data_ctor(&sim);


    int nameLen, size, rank;
    char processorName[MPI_MAX_PROCESSOR_NAME];
    MPI_Comm comm = initMpi(argc, argv, flagReader.getCyclic(), flagReader.getProcessRow(), flagReader.getProcessCol(), flagReader.getProcessReorder());
    MPI_Get_processor_name(processorName, &nameLen);
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    
    
    int stepNum, width, length;
    if(rank == 0){
        if(!flagReader.getNoInput()){
            width = userInput("Please enter the width of the cells's matrix for each process: ", "The width of the matrix must be > zero!\n", 0);
            length = userInput("Please enter the length of the cells's matrix for each process: ", "The length of the matrix must be > zero!\n", 0);
            stepNum = userInput("Please enter the number of generations: ", "The number of generations must be >= zero!\n", -1);
        }
        else{
            width = flagReader.getMatrixRow(), length = flagReader.getMatrixCol(), stepNum = flagReader.getGenerations();
        }
    }

    MPI_Bcast(&width, 1, MPI_INT, 0, comm);
    MPI_Bcast(&length, 1, MPI_INT, 0, comm);
    MPI_Bcast(&stepNum, 1, MPI_INT, 0, comm);
    int maxStepNum = stepNum;

    CellMatrix cellMatrix(width, length, flagReader.getCyclic(), flagReader.getCommunication(), flagReader.getDemoCalc(), rank, size, nameLen, comm, flagReader.getProcessRow(), flagReader.getProcessCol(), flagReader.getThreads());
    cellMatrix.initilizeRandomMatrix(flagReader.getRandom());
    cellMatrix.setNeighbors(allNeighborsProc(comm, flagReader.getCyclic(), flagReader.getProcessRow(), flagReader.getProcessCol()));
    
    
    
    /* Initialize environment variables. */
    VisItSetupEnvironment();
    /* Install callback functions for global communication. */
    VisItSetBroadcastIntFunction2(visit_broadcast_int_callback, (void*)&cellMatrix);
    VisItSetBroadcastStringFunction2(visit_broadcast_string_callback, (void*)&cellMatrix);
    /* Tell libsim whether the simulation is parallel. */
    VisItSetParallel(size > 1);
    VisItSetParallelRank(rank);
    /* Only read the environment on rank 0. This could happen before MPI_Init if
     * we are using an MPI that does not like to let us spawn processes but we
     * would not know our processor rank.
     */
    if(rank == 0){
        env = VisItGetEnvironment();
    }
    /* Pass the environment to all other processors collectively. */
    VisItSetupEnvironment2(env);
    if(env != NULL){
        free(env);
    }
    /* Init libsim. */
    if(rank == 0)
    {
        VisItInitializeSocketAndDumpSimFile("game_of_life", "Demonstrates game of life interactive mode", "/path/to/where/sim/was/started", NULL, "game_of_life.ui", SimulationFilename());
    }
    

    // plotData(40, flagReader.getPlot(), maxStepNum, cellMatrix);
    
    
    /* Call the main loop. */
    mainloop_batch(maxStepNum, &cellMatrix, &flagReader);
    
    cellMatrix.freeMemory();
    /* Free simulation struct. */
    // simulation_data_dtor(&sim);
    
    MPI_Finalize();
    return 0;
}