#!/bin/bash

let processRow=8
let processCol=4
let processNum=$processRow*$processCol
let genNum=25

rm -f times.txt

echo "processRow: "$processRow", processCol: "$processCol", generationsNum: "$genNum >> times.txt

for threadNum in 1
do
    for matrixSize in 12000 14000 16000 18000
    do
        let rowSize=$matrixSize #/$processRow
        let colSize=$matrixSize #/$processCol
        echo -n $threadNum" threads- "$matrixSize" matrix size: " >> times.txt
        echo ""
        \time -ao times.txt mpirun -n $processNum ./build/game_of_life row=$processRow col=$processCol matrixr=$rowSize matrixc=$colSize gen=$genNum threads=$threadNum reorder=1 -n
        echo ""
    done
done

#python game_of_life_scale.py


# --bind-to core --map-by ppr:4:numa:PE=3


# -f "%E"