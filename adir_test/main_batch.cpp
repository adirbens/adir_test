#include <iostream>
#include <game_of_life.h>
#include <mpi.h>
#include <VisItControlInterface_V2.h>
#include <VisItDataInterface_V2.h>
#include <string.h>
using namespace std;





#define SIM_STOPPED       0
#define SIM_RUNNING       1




static int visit_broadcast_int_callback(int *value, int sender, void *cbdata)
{
    return MPI_Bcast(value, 1, MPI_INT, sender, MPI_COMM_WORLD);
}

static int visit_broadcast_string_callback(char *str, int len, int sender, void *cbdata)
{
    // simulation_data *sim = (simulation_data *)cbdata;
    return MPI_Bcast(str, len, MPI_CHAR, sender, MPI_COMM_WORLD);
}






visit_handle SimGetMetaData(void *cbdata)
{
    visit_handle md = VISIT_INVALID_HANDLE;
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;

    /* Create metadata. */
    if(VisIt_SimulationMetaData_alloc(&md) == VISIT_OKAY)
    {
        visit_handle mmd = VISIT_INVALID_HANDLE;
        visit_handle vmd = VISIT_INVALID_HANDLE;

        /* Set the simulation state. */
        VisIt_SimulationMetaData_setMode(md, (cellMatrix->getRunMode() == SIM_STOPPED) ?
            VISIT_SIMMODE_STOPPED : VISIT_SIMMODE_RUNNING);
        VisIt_SimulationMetaData_setCycleTime(md, cellMatrix->getCycle(), cellMatrix->getTime());

        /* Fill in the mesh metadata. */
        if(VisIt_MeshMetaData_alloc(&mmd) == VISIT_OKAY)
        {
            /* Set the mesh's properties.*/
            VisIt_MeshMetaData_setName(mmd, "cells_mesh");
            VisIt_MeshMetaData_setMeshType(mmd, VISIT_MESHTYPE_RECTILINEAR);
            VisIt_MeshMetaData_setTopologicalDimension(mmd, 2);
            VisIt_MeshMetaData_setSpatialDimension(mmd, 2);

            int ndoms = cellMatrix->getSize();  // ?????
            VisIt_MeshMetaData_setNumDomains(mmd, ndoms);
            VisIt_MeshMetaData_setDomainTitle(mmd, "patches");
            VisIt_MeshMetaData_setDomainPieceName(mmd, "patch");

            VisIt_SimulationMetaData_addMesh(md, mmd);
        }

        /* Add a variable. */
        if(VisIt_VariableMetaData_alloc(&vmd) == VISIT_OKAY)
        {
            VisIt_VariableMetaData_setName(vmd, "cells");
            VisIt_VariableMetaData_setMeshName(vmd, "cells_mesh");
            VisIt_VariableMetaData_setType(vmd, VISIT_VARTYPE_SCALAR);
            VisIt_VariableMetaData_setCentering(vmd, VISIT_VARCENTERING_ZONE);

            VisIt_SimulationMetaData_addVariable(md, vmd);
        }
    }
    return md;
}

visit_handle SimGetMesh(int domain, const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;

    /* Get the patch with the appropriate domain id. */
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;

    if(strcmp(name, "cells_mesh") == 0 && cellMatrix->getRank() == domain)
    {
        int i;

        if(VisIt_RectilinearMesh_alloc(&h) != VISIT_ERROR)
        {
            /* Initialize X coords. */
            float *coordX = (float *)malloc(sizeof(float) * (cellMatrix->getLength() + 1));
            int xOffset = (cellMatrix->getRank() % cellMatrix->getMeshCol()) * cellMatrix->getLength();
            for(i = 0; i < cellMatrix->getLength() + 1; i++)
            {
                coordX[i] = i + xOffset;
            }
            /* Initialize Y coords. */
            float *coordY = (float *)malloc(sizeof(float) * (cellMatrix->getWidth() + 1));
            int yOffset = (cellMatrix->getRank() / cellMatrix->getMeshRow()) * cellMatrix->getWidth();
            for(i = 0; i < cellMatrix->getWidth() + 1; i++)
            {
                coordY[i] = i + yOffset;
            }

            /* Give the mesh some coordinates it can use. */
            visit_handle xc, yc, gs;
            VisIt_VariableData_alloc(&xc);
            VisIt_VariableData_alloc(&yc);
            VisIt_VariableData_alloc(&gs);
            if(xc != VISIT_INVALID_HANDLE && yc != VISIT_INVALID_HANDLE)
            {
                VisIt_VariableData_setDataF(xc, VISIT_OWNER_VISIT, 1, cellMatrix->getLength() + 1, coordX);
                VisIt_VariableData_setDataF(yc, VISIT_OWNER_VISIT, 1, cellMatrix->getWidth() + 1, coordY);
                VisIt_RectilinearMesh_setCoordsXY(h, xc, yc);
                
                // int min[] = {1, 1, 0}; // BE CAREFUL HERE
                // int max[] = {cellMatrix->getLength() + 1, cellMatrix->getWidth() + 1, 1}; // BE CAREFUL HERE
                // VisIt_RectilinearMesh_setRealIndices(h, min, max);  // BE CAREFUL HERE
            }
            else
            {
                free(coordX);
                free(coordY);
            }
        }
    }

    return h;
}

int* getMatrixNoGhosts(CellMatrix* cellMatrix){
    int* matrixNoGhosts = (int*)malloc(sizeof(int) * cellMatrix->getLength() * cellMatrix->getWidth());
    for(int i = 0; i < cellMatrix->getWidth(); i++){
        for(int j = 0; j < cellMatrix->getLength(); j++){
            matrixNoGhosts[i * cellMatrix->getWidth() + j] = cellMatrix->getCell(i + 1, j + 1);
        }
    }
    return matrixNoGhosts;
}

visit_handle SimGetVariable(int domain, const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;

    /* Get the patch with the appropriate domain id. */
    CellMatrix *cellMatrix = (CellMatrix *)cbdata;

    if(strcmp(name, "cells") == 0 && cellMatrix->getRank() == domain)
    { 
        VisIt_VariableData_alloc(&h);
        VisIt_VariableData_setDataI(h, VISIT_OWNER_VISIT, 1,
            cellMatrix->getLength() * cellMatrix->getWidth(), getMatrixNoGhosts(cellMatrix));
    }

    return h;
}

visit_handle SimGetDomainList(const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;
    if(VisIt_DomainList_alloc(&h) != VISIT_ERROR)
    {
        visit_handle hdl = VISIT_INVALID_HANDLE;
        CellMatrix *cellMatrix = (CellMatrix *)cbdata;
        //int npatches = patch_num_patches(&sim->patch);
        
        int * rank = (int*)malloc(sizeof(int));
        rank[0] = cellMatrix->getRank();
        //if(sim->npatch_list > 0)
        //{
        VisIt_VariableData_alloc(&hdl);
        VisIt_VariableData_setDataI(hdl, VISIT_OWNER_SIM, 1, 1, rank);
        //}

        VisIt_DomainList_setDomains(h, cellMatrix->getSize(), hdl);
    }
    return h;
}




void mainloop_batch(int maxStepNum, CellMatrix* cellMatrix, FlagReader* flagReader){

    /* Explicitly load VisIt runtime functions and install callbacks. */
    VisItInitializeRuntime();
    VisItSetGetMetaData(SimGetMetaData, (void*)cellMatrix);
    VisItSetGetMesh(SimGetMesh, (void*)cellMatrix);
    VisItSetGetVariable(SimGetVariable, (void*)cellMatrix);
    VisItSetGetDomainList(SimGetDomainList, (void*)cellMatrix);

    int stepNum = 0;
    while(stepNum <= maxStepNum){
    
        VisItTimeStepChanged();
        
        // add a pseudocolor plot
        VisItAddPlot("Pseudocolor", "cells");
        VisItAddPlot("Mesh", "cells_mesh");
        VisItDrawPlots();
        VisItUpdatePlots();

        // save file
        char filename[100];
        sprintf(filename, "cells_%06d.png", stepNum);
        if(VisItSaveWindow(filename, 1000, 1000, VISIT_IMAGEFORMAT_PNG) == VISIT_OKAY)
        {
            if(cellMatrix->getRank() == 0){
                printf("Rendered to %s\n", filename);
            }
        }
        else
        {
            if(cellMatrix->getRank() == 0){
                printf("Failed to save %s\n", filename);
            }
        }

        if(stepNum % 10 == 0)
        {
            // export data to every 10th cycle
            visit_handle vars = VISIT_INVALID_HANDLE;
            VisIt_NameList_alloc(&vars);
            VisIt_NameList_addName(vars, "default");
            sprintf(filename, "cells_export_%06d", stepNum);
            VisItExportDatabase(filename, "VTK_1.0", vars);
            if(cellMatrix->getRank() == 0){
                printf("Exported to %s\n", filename);
            }
            VisIt_NameList_free(vars);
        }
    
        writeMatrix(stepNum, maxStepNum, flagReader->getFile(), flagReader->getTerminal(), cellMatrix, flagReader->getProcessRow(), flagReader->getProcessCol());
        cellMatrix->fullStep();
        
        VisItDeleteActivePlots();
        VisItDeleteActivePlots();
        
        stepNum++;
    }

}





int main(int argc, char *argv[]){

    FlagReader flagReader(0);
    flagReader.initialFlags(argc, argv);

    
    /* Initialize environment and simulation struct. */
    char *env = NULL;
    // simulation_data sim;
    // simulation_data_ctor(&sim);


    int nameLen, size, rank;
    char processorName[MPI_MAX_PROCESSOR_NAME];
    MPI_Comm comm = initMpi(argc, argv, flagReader.getCyclic(), flagReader.getProcessRow(), flagReader.getProcessCol(), flagReader.getProcessReorder());
    MPI_Get_processor_name(processorName, &nameLen);
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    
    
    int stepNum, width, length;
    if(rank == 0){
        if(!flagReader.getNoInput()){
            width = userInput("Please enter the width of the cells's matrix for each process: ", "The width of the matrix must be > zero!\n", 0);
            length = userInput("Please enter the length of the cells's matrix for each process: ", "The length of the matrix must be > zero!\n", 0);
            stepNum = userInput("Please enter the number of generations: ", "The number of generations must be >= zero!\n", -1);
        }
        else{
            width = flagReader.getMatrixRow(), length = flagReader.getMatrixCol(), stepNum = flagReader.getGenerations();
        }
    }

    MPI_Bcast(&width, 1, MPI_INT, 0, comm);
    MPI_Bcast(&length, 1, MPI_INT, 0, comm);
    MPI_Bcast(&stepNum, 1, MPI_INT, 0, comm);
    int maxStepNum = stepNum;

    CellMatrix cellMatrix(width, length, flagReader.getCyclic(), flagReader.getCommunication(), flagReader.getDemoCalc(), rank, size, nameLen, comm, flagReader.getProcessRow(), flagReader.getProcessCol(), flagReader.getThreads());
    cellMatrix.initilizeRandomMatrix(flagReader.getRandom());
    cellMatrix.setNeighbors(allNeighborsProc(comm, flagReader.getCyclic(), flagReader.getProcessRow(), flagReader.getProcessCol()));
    
    
    
    /* Initialize environment variables. */
    VisItSetupEnvironment();
    /* Install callback functions for global communication. */
    VisItSetBroadcastIntFunction2(visit_broadcast_int_callback, (void*)&cellMatrix);
    VisItSetBroadcastStringFunction2(visit_broadcast_string_callback, (void*)&cellMatrix);
    /* Tell libsim whether the simulation is parallel. */
    VisItSetParallel(size > 1);
    VisItSetParallelRank(rank);
    /* Only read the environment on rank 0. This could happen before MPI_Init if
     * we are using an MPI that does not like to let us spawn processes but we
     * would not know our processor rank.
     */
    if(rank == 0){
        env = VisItGetEnvironment();
    }
    /* Pass the environment to all other processors collectively. */
    VisItSetupEnvironment2(env);
    if(env != NULL){
        free(env);
    }
    /* Init libsim. */
    if(rank == 0)
    {
        VisItInitializeRuntime();
    }
    
    
    

    // plotData(40, flagReader.getPlot(), maxStepNum, cellMatrix);
    
    
    /* Call the main loop. */
    mainloop_batch(maxStepNum, &cellMatrix, &flagReader);
    
    cellMatrix.freeMemory();
    /* Free simulation struct. */
    // simulation_data_dtor(&sim);
    
    MPI_Finalize();
    return 0;
}