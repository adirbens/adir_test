#include <mpi.h>
#include <map>
using namespace std;
#define SIM_STOPPED       0
#define SIM_RUNNING       1

class CellMatrix{
  private:
    int width;
    int length;
    int cyclic;
    int hasCopy;
    int rank;
    int size;
    int meshRow;
    int meshCol;
    int nameLen;
    int threads;
    int communication;
    int demoCalc;
    int     cycle;
    double  time;
    int     runMode;
    int     done;
    int changed;
    int** matrix;
    int** originMatrix;
    MPI_Comm comm;
    int * realMatrix;
    MPI_Datatype columntype;
    map<string, int> neighbors;
  public:
    CellMatrix(int widthVal, int lengthVal, int cyclicVal, int communicationVal, int demoCalcVal, int rankVal, int sizeVal, int nameLenVal, MPI_Comm commVal, int meshRowVal, int meshColVal, int threadsVal=0){
        width = widthVal;
        length = lengthVal;
        cyclic = cyclicVal;
        rank = rankVal;
        size = sizeVal;
        meshRow = meshRowVal;
        meshCol = meshColVal;
        nameLen = nameLenVal;
        comm = commVal;
        communication = communicationVal;
        demoCalc = demoCalcVal;
        hasCopy = 0;
        threads = threadsVal;
        cycle = 0;
        time = 0;
        done = 0;
        changed = 1;
        runMode = SIM_STOPPED;
        MPI_Type_vector(width, 1, length + 2, MPI_INT, &columntype);
        MPI_Type_commit(&columntype);
    }
    int getCyclic(){ return cyclic; }
    int getWidth(){ return width; }
    int getLength(){ return length; }
    int getRank(){ return rank; }
    int getSize(){ return size; }
    int getCommunication() { return communication; }
    int getDemoCalc() {return demoCalc; }
    int getCell(int row, int column){ return matrix[row][column]; }
    int getCycle(){ return cycle; }
    int getTime(){ return time; }
    int getRunMode(){ return runMode; }
    int getDone(){ return done; }
    int getMeshRow(){ return meshRow; }
    int getMeshCol(){ return meshCol; }
    int getChanged(){ return changed; }
    int* getMatrix(){ return realMatrix; }
    void setDone(int newDone){ done = newDone; }
    void setRunMode(int newRunMode){ runMode = newRunMode; }
    void setNeighbors(map<string, int> neighborsVal){ neighbors = neighborsVal; }

    /**
     * Initializing matrix with dead/alive cells.
     *
     * @param random the flag that determines if we generate random values
     *      at every run, or use the same initial random values (can be
     *      a good use for testing).
     */
    void initilizeRandomMatrix(int random);

    /**
     * Initializing the matrix as a copy of the originMatrix.
     * 
     */
    void initilizeFromOrigin();

    /**
     * Initializing costume matrix for testing,
     * for more details go to the test documentation.
     *
     * * @param mod the value that helps build the costume matrix.
     */
    void initilizeCostumeMatrix(int mod);

    /**
     * Checking if the neighbor matrix[i][j] is alive and valid.
     *
     * @param i the row of the neighbor.
     * @param j the column of the neighbor.
     * @return 1 if the neighbor is within the boundaries of the matrix,
     *      and 0 otherwise.
     */
    int isNeighbor(int i, int j);

    /**
     * Counting the neighbors of a given cell.
     *
     * @param i the row of the given cell.
     * @param j the column of the given cell.
     * @return the number of the neighbors of the given cell (0-8).
     */
    int neighborsNumber(int i, int j);

    /**
     * Fixing the edges points (the padding) of the matrix (relevant only foruse of 1 process total)
     * 
     */
    void fixEdges();

    /**
     * Fixing the ghost points of the matrix according to the neighbors matrices.
     * 
     */
    void updateGhostPoints();

    /**
     * Fixing the matrix's values to 0/1's,
     * according to the rules explained in the implementation.
     *
     */
    void fixMatrix();

    /**
     * Performing a full generation step.
     *
     */
    void fullStep();

    /**
     * Deleting the memory allocated for the matrix.
     *
     */
    void freeMemory();
};

class FlagReader{
  private:
    int cyclic;
    int random;
    int terminal;
    int file;
    int plot;
    int noInput;
    int processRow;
    int processCol;
    int processReorder;
    int matrixRow;
    int matrixCol;
    int generations;
    int threads;
    int communicTime;
    int demoCalc;
    int batch;
    int interactive;
  public:
    FlagReader(int defaultVal){
        cyclic = defaultVal;
        random = defaultVal;
        terminal = defaultVal;
        file = defaultVal;
        plot = defaultVal;
        processReorder = defaultVal;
        noInput = defaultVal;
        communicTime = defaultVal;
        demoCalc = defaultVal;
        batch = defaultVal;
        interactive = defaultVal;
        processRow = 3;
        processCol = 3;
        matrixRow = 3;
        matrixCol = 3;
        generations = 3;
        threads = 1;
    }
    int getCyclic(){ return cyclic; }
    int getRandom(){ return random; }
    int getTerminal(){ return terminal; }
    int getFile(){ return file; }
    int getPlot(){ return plot; }
    int getNoInput(){ return noInput; }
    int getProcessRow(){ return processRow; }
    int getProcessCol(){ return processCol; }
    int getProcessReorder(){ return processReorder; }
    int getMatrixRow(){ return matrixRow; }
    int getMatrixCol(){ return matrixCol; }
    int getGenerations(){ return generations; }
    int getThreads(){ return threads; }
    int getCommunication() { return communicTime; }
    int getDemoCalc() { return demoCalc; }
    int getBatch() { return batch; }
    int getInteractive() { return interactive; }
    void cyclicOn(){ cyclic = 1; }
    void randomOn(){ random = 1; }
    void terminalOn(){ terminal = 1; }
    void fileOn(){ file = 1; }
    void plotOn(){ plot = 1; }
    void communicationOn() { communicTime = 1; }
    void demoCalcOn() { demoCalc = 1; }
    void noInputOn(){ noInput = 1; }
    void batchOn() { batch = 1; }
    void interactiveOn() { interactive = 1; }
    void SetProcessRow(int value){ processRow = value; }
    void SetProcessCol(int value){ processCol = value; }
    void SetProcessReorder(int value){ processReorder = value; }
    void SetMatrixRow(int value){ matrixRow = value; }
    void SetMatrixCol(int value){ matrixCol = value; }
    void SetGenerations(int value){ generations = value; }
    void SetThreads(int value){ threads = value; }

    /**
     * Initializing the flags with 0/1's, according
     * to arguments given.
     *
     * @param argc the number of arguments.
     * @param argv the arguments.
     */
    void initialFlags(int argc, char *argv[]);
};


/**
 * Helper function, takes input from the user.
 * 
 * @param instruction the instruction for the user.
 * @param failure the failure message for the user, if needed.
 * @param limit the lowest alloweded value.
 * @return the legal value from the user.
 */
int userInput(string instruction, string failure, int limit);

/**
 * Helper function, used to perform the writing to cout and/or to an output file.
 *
 * @param toTerminal writing to terminal flag.
 * @param toFile writing to file flag.
 * @param outFile the output file.
 * @param message the message to write.
 * @param color the color of the message.
 * @param isNewline newline flag.
 */
void writeCoutAndFile(int toTerminal, int toFile, ofstream &outFile, string message, string color, int isNewline);

/**
 * Writing the values of the matrix to an output file and/or the terminal.
 *
 * @param genNum the number of the generation.
 * @param toFile writing to file flag.
 * @param toTerminal writing to terminal flag.
 * @param cellMatrix the cell matrix object.
 */
void writeMatrix(int genNum, int maxGenNum, int toFile, int toTerminal, CellMatrix* cellMatrix, int processRow, int processCol);  //FIX DOC

void writeGhostPoints(CellMatrix matrix);

/**
 * Writing running data to an output file.
 * 
 * @param maxThreadsNum the maximun number of threads that we want to use.
 * @param plot writing to file flag.
 * @param stepNum the number of generations.
 * @param cellMatrix the cell matrix object.
 */
void plotData(int maxThreadsNum, int plot, int stepNum, CellMatrix &cellMatrix);

/**
 * Finding the rank of a wanted process.
 * 
 * @param comm the communicator that we use.
 * @param right the right shift from the current process (can be 1/-1).
 * @param down the down shift from the current process (can be 1/-1).
 * @return the wanted rank.
 */
int diagonalNeighborProc(MPI_Comm comm, int right, int down);

/**
 * Creating a map which contains all neighbords from each direction.
 * 
 * @param comm the communicator that we use.
 * @param cyclic periodic matrix flag.
 * @return the neighbors map.
 */
map<string, int> allNeighborsProc(MPI_Comm comm, int cyclic, int processRow, int processCol);

/**
 * Initializing all MPI related configurations.
 * 
 * @param argc 
 * @param argv 
 * @param periodic periodic matrix flag.
 * @param rows rows number for our process matrix.
 * @param cols cols number for our process matrix.
 * @param reorder reorder process matrix flag.
 * @return the communicator that we use. 
 */
MPI_Comm initMpi(int argc, char *argv[], int periodic, int rows, int cols, int reorder);