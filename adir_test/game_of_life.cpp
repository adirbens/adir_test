#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <ctime>
// #include <time.h>
#include <fstream>
#include <filesystem>
#include <string.h>
#include <omp.h>
#include <mpi.h>
#include <chrono>
#include <map>
#include <game_of_life.h>

#define RED "\033[31m"
#define GREEN "\033[32m"
#define BOLDWHITE "\033[1m\033[37m"
#define RESET "\033[0m"
#define CYCLIC "-c"
#define RANDOM "-r"
#define TERMINAL "-t"
#define FILE "-f"
#define PLOT "-p"
#define NOINPUT "-n"
#define COMMUNICATION "-m"
#define DEMO "-d"
#define BATCH "-b"
#define INTERACTIVE "-i"

using namespace std;


void CellMatrix::initilizeFromOrigin(){
    for(int i = 0; i < width + 2; i++){
        for(int j = 0; j < length + 2; j++){
            matrix[i][j] = originMatrix[i][j];
        }
    }
    time = 0;
    //if(cyclic){
    //    fixEdges();
    //}
}

void CellMatrix::initilizeRandomMatrix(int random){
    int numProcs;
    MPI_Comm_size(comm, &numProcs);
    if(random){
        srand((unsigned)time + rank * numProcs + nameLen); // FIX THE ISSUE WITH THE TIME (FIELD & FUNCTION)
    }
    hasCopy = 1;
    matrix = new int *[width + 2];
    realMatrix = new int[(width + 2) * (length + 2)];
    for(int i = 0; i < width + 2; i++){
        matrix[i] = &realMatrix[0 + i * (length + 2)];
    }
    originMatrix = new int *[width + 2];
    for(int i = 0; i < width + 2; i++){
        originMatrix[i] = new int[length + 2];
        for(int j = 0; j < length + 2; j++){
            matrix[i][j] = rand() % 2;
            originMatrix[i][j] = matrix[i][j];
            if(i == 0 or i == width + 1 or j == 0 or j == length + 1){
                matrix[i][j] = 0; originMatrix[i][j] = 0;
            }
        }
    }
    //if(cyclic){
    //    fixEdges();
    //}
    //updateGhostPoints();
}

void CellMatrix::initilizeCostumeMatrix(int mod){
  int k = 1;
  matrix = new int *[width + 2];
  realMatrix = new int[(width + 2) * (length + 2)];
  for(int i = 0; i < width + 2; i++){
    matrix[i] = &realMatrix[0 + i * (length + 2)];
  }
  for(int i = 0; i < width + 2; i++){
    for(int j = 0; j < length + 2; j++){
        if(i == 0 or i == width + 1 or j == 0 or j == length + 1){
            matrix[i][j] = 0;
            continue;
        }
        if(k++ % mod == 0){
            matrix[i][j] = 0;
        }
        else{
            matrix[i][j] = 1;
        }
    }
  }
  //if(cyclic){
  //  fixEdges();
  //}
}

int CellMatrix::isNeighbor(int i, int j){
    if (0 <= i and i < width + 2 and 0 <= j and j < length + 2){
        if (matrix[i][j] == 1 or matrix[i][j] == 3){
            return 1;
        }
    }
    return 0;
}

int CellMatrix::neighborsNumber(int i, int j){
    return CellMatrix::isNeighbor(i + 1, j) + CellMatrix::isNeighbor(i - 1, j) + 
        CellMatrix::isNeighbor(i, j + 1) + CellMatrix::isNeighbor(i, j - 1) + 
        CellMatrix::isNeighbor(i + 1, j + 1) + CellMatrix::isNeighbor(i - 1, j - 1) + 
        CellMatrix::isNeighbor(i + 1, j - 1) + CellMatrix::isNeighbor(i - 1, j + 1);
}

void CellMatrix::fixEdges(){
    int i;
    for(i = 1; i < length + 1; i++){
        matrix[0][i] = matrix[width][i];
        matrix[width + 1][i] = matrix[1][i];
    }
    for(i = 1; i < width + 1; i++){
        matrix[i][0] = matrix[i][length];
        matrix[i][length + 1] = matrix[i][1];
    }
    matrix[0][0] = matrix[width][length];
    matrix[width + 1][length + 1] = matrix[1][1];
    matrix[0][length + 1] = matrix[width][1];
    matrix[width + 1][0] = matrix[1][length];
}

void CellMatrix::updateGhostPoints(){
    chrono::high_resolution_clock::time_point start;
    ofstream outFile;
    if(communication){
        std::filesystem::create_directories("communicationTimes");
        string fileName = "communicationTimes/output" + to_string(rank) + ".txt";
        outFile.open(fileName, ios_base::app);
        start = chrono::high_resolution_clock::now();
    }
    //top-bottom exchange
    MPI_Sendrecv(&matrix[1][1], length, MPI_INT, neighbors["t"], 0, &matrix[width + 1][1], length, MPI_INT, neighbors["b"], 0, comm, MPI_STATUS_IGNORE);
    MPI_Sendrecv(&matrix[width][1], length, MPI_INT, neighbors["b"], 1, &matrix[0][1], length, MPI_INT, neighbors["t"], 1, comm, MPI_STATUS_IGNORE);
    //4-edges exchange
    MPI_Sendrecv(&matrix[1][1], 1, MPI_INT, neighbors["l-t"], 2, &matrix[width + 1][length + 1], 1, MPI_INT, neighbors["r-b"], 2, comm, MPI_STATUS_IGNORE);
    MPI_Sendrecv(&matrix[width][length], 1, MPI_INT, neighbors["r-b"], 3, &matrix[0][0], 1, MPI_INT, neighbors["l-t"], 3, comm, MPI_STATUS_IGNORE);
    MPI_Sendrecv(&matrix[1][length], 1, MPI_INT, neighbors["r-t"], 4, &matrix[width + 1][0], 1, MPI_INT, neighbors["l-b"], 4, comm, MPI_STATUS_IGNORE);
    MPI_Sendrecv(&matrix[width][1], 1, MPI_INT, neighbors["l-b"], 5, &matrix[0][length + 1], 1, MPI_INT, neighbors["r-t"], 5, comm, MPI_STATUS_IGNORE);
    //left-right exchange
    MPI_Sendrecv(&matrix[1][1], 1, columntype, neighbors["l"], 6, &matrix[1][length + 1], 1, columntype, neighbors["r"], 6, comm, MPI_STATUS_IGNORE);
    MPI_Sendrecv(&matrix[1][length], 1, columntype, neighbors["r"], 7, &matrix[1][0], 1, columntype, neighbors["l"], 7, comm, MPI_STATUS_IGNORE);
    if(communication){
        auto end = chrono::high_resolution_clock::now();
        outFile << chrono::duration_cast<chrono::milliseconds>(end - start).count() / (1000.0) << "\n";
    }
}

/**
 * Updating the matrix values according to the following rules:
 * (explained in fullStep documentation)
 *
 * 0 -> 0
 * 1 -> 0
 * 2 -> 1
 * 3 -> 1
 */
void CellMatrix::fixMatrix(){
    changed = 0;
    #ifdef OUTER
    #pragma omp parallel for num_threads(threads)
    #endif
    for(int i = 1; i < width + 1; i++){
        #ifdef INNER
        #pragma omp parallel for
        #endif
        for(int j = 1; j < length + 1; j++){
            if (matrix[i][j] == 1 or matrix[i][j] == 2)
            {
                changed += 1;
            }
            if(matrix[i][j] == 1){
                matrix[i][j] = 0;
            }
            else if(matrix[i][j] == 2 or matrix[i][j] == 3){
                matrix[i][j] = 1;
            }
        }
    }
    //if(cyclic){
    //    fixEdges();
    //}
}

/**
 * In order to not use another O(n*m) memory, we will use the same
 * matrix and the following rules:
 *
 * dead now -> dead next generation - marked with the value 0
 * alive now -> dead next generation - marked with the value 1
 * dead now -> alive next generation - marked with the value 2
 * alive now -> alive next generation - mark with the value 3
 */
void CellMatrix::fullStep(){
    updateGhostPoints();
    #ifdef OUTER
    #pragma omp parallel for num_threads(threads)
    #endif
    for(int i = 1; i < width + 1; i++){
        #ifdef INNER
        #pragma omp parallel for
        #endif
        for(int j = 1; j < length + 1; j++){
            if(demoCalc){
                matrix[i][j] = 1 - matrix[i][j];
            }
            else{
                int neighbors = neighborsNumber(i, j);
                if(matrix[i][j] == 0){
                    if (neighbors == 3){
                        matrix[i][j] = 2;
                    }
                }
                else{
                    if(neighbors == 2 or neighbors == 3){
                        matrix[i][j] = 3;
                    }
                }
            }
        }
    }
    fixMatrix();
    //updateGhostPoints();
    cycle++;
    time++;
}

void CellMatrix::freeMemory(){
    delete matrix;
    delete realMatrix;
    if(hasCopy){
        for(int i = 0; i < width + 2; i++){
            delete originMatrix[i];
        }
        delete originMatrix;
    }
    MPI_Type_free(&columntype);
}


int extractValue(string flag, int length){
    string value = flag.substr(length, flag.find("="));
    return stoi(value);
}

void FlagReader::initialFlags(int argc, char *argv[]){
    for(int i = 1; i < argc; i++){
        string arg = argv[i];
        if(strcmp(argv[i], CYCLIC) == 0){
            FlagReader::cyclicOn();
        }
        else if(strcmp(argv[i], RANDOM) == 0){
            FlagReader::randomOn();
        }
        else if(strcmp(argv[i], FILE) == 0){
            FlagReader::fileOn();
        }
        else if(strcmp(argv[i], TERMINAL) == 0){
            FlagReader::terminalOn();
        }
        else if(strcmp(argv[i], PLOT) == 0){
            FlagReader::plotOn();
        }
        else if(strcmp(argv[i], NOINPUT) == 0){
            FlagReader::noInputOn();
        }
        else if(strcmp(argv[i], COMMUNICATION) == 0){
            FlagReader::communicationOn();
        }
        else if(strcmp(argv[i], DEMO) == 0){
            FlagReader::demoCalcOn();
        }
        else if(strcmp(argv[i], BATCH) == 0){
            FlagReader::batchOn();
        }
        else if(strcmp(argv[i], INTERACTIVE) == 0){
            FlagReader::interactiveOn();
        }
        else if(arg.find("row=") != string::npos){
            FlagReader::SetProcessRow(extractValue(arg, 4));
        }
        else if(arg.find("col=") != string::npos){
            FlagReader::SetProcessCol(extractValue(arg, 4));
        }
        else if(arg.find("reorder=") != string::npos){
            FlagReader::SetProcessReorder(extractValue(arg, 8));
        }
        else if(arg.find("matrixr=") != string::npos){
            FlagReader::SetMatrixRow(extractValue(arg, 8));
        }
        else if(arg.find("matrixc=") != string::npos){
            FlagReader::SetMatrixCol(extractValue(arg, 8));
        }
        else if(arg.find("gen=") != string::npos){
            FlagReader::SetGenerations(extractValue(arg, 4));
        }
        else if(arg.find("threads=") != string::npos){
            FlagReader::SetThreads(extractValue(arg, 8));
        }
    }
}


int userInput(string instruction, string failure, int limit){
    int value;
    cout << instruction;
    cin >> value;
    if(value <= limit){
        cout << failure;
        while(value <= limit){
            cout << "Please try again: ";
            cin >> value;
        }
    }
    return value;
}

void writeCoutAndFile(int toTerminal, int toFile, ofstream &outFile, string message, string color, int isNewline){
    if(isNewline){
        if(toTerminal){
		    cout << "\n";
        }
        if(toFile){
            outFile << "\n";
        }
    }
    else{
        if(toTerminal){
            cout << color << message << RESET;
        }
        if(toFile){
            outFile << message;
        }
    }
}

void writeMatrix(int genNum, int maxGenNum, int toFile, int toTerminal, CellMatrix* cellMatrix, int processRow, int processCol){
    if(toFile or toTerminal){
        ofstream outFile;
        if(toTerminal){
            cout << "\nGeneration number: " << BOLDWHITE << genNum << "/" << maxGenNum << RESET << ", rank:" << cellMatrix->getRank() << ", processes:" << processRow << "x" << processCol << "\n";
        }
        if(toFile){
            std::filesystem::create_directories("output");
            string name;
            name = "output/output" + to_string(cellMatrix->getRank()) + ".txt";
            outFile.open(name, ios_base::app);
            outFile << "Generation number:" << genNum << "/" << maxGenNum << ", rank:" << cellMatrix->getRank() << ", matrix:" << cellMatrix->getWidth() << "x" << cellMatrix->getLength() << ", processes:" << processRow << "x" << processCol << "\n";
        }
        for(int i = 1; i < cellMatrix->getWidth() + 1; i++){
            for(int j = 1; j < cellMatrix->getLength() + 1; j++){
                if(cellMatrix->getCell(i, j) == 1){
                    writeCoutAndFile(toTerminal, toFile, outFile, "O ", GREEN, 0);
                }
                else{
                    writeCoutAndFile(toTerminal, toFile, outFile, "X ", RED, 0);
                }
            }
            writeCoutAndFile(toTerminal, toFile, outFile, "", "", 1);
        }
        if(toTerminal){
            sleep(1);
        }
        writeCoutAndFile(toTerminal, toFile, outFile, "", "", 1);
        fflush(stdout);
    }
}

void writeGhostPoints(CellMatrix matrix){
    for(int i = 0; i < matrix.getLength() + 2; i++){
        cout << matrix.getCell(0, i);
    }
    cout << "\n";
    for(int i = 1; i < matrix.getWidth() + 1; i++){
        cout << matrix.getCell(i, 0);
        for(int j = 0; j < matrix.getLength(); j++){
            cout << " ";
        }
        cout << matrix.getCell(i, matrix.getLength() + 1);
        cout << "\n";
    }
    for(int i = 0; i < matrix.getLength() + 2; i++){
        cout << matrix.getCell(matrix.getWidth() + 1, i);
    }
    cout << "\n";
}

void plotData(int maxThreadsNum, int plot, int stepNum, CellMatrix &cellMatrix){
    if(plot and cellMatrix.getRank() == 0){ // CHANGE THAT!!!
        ofstream outFile;
        outFile.open("plotData.txt", ios_base::app);
        outFile << cellMatrix.getWidth() << "x" << cellMatrix.getLength() << "x" << stepNum;
        #ifdef OUTER
        outFile << "xouter";
        #endif
        #ifdef INNER
        outFile << "xinner";
        #endif
        for(int currThreadNum = 16; currThreadNum <= maxThreadsNum; currThreadNum++){ // CHANGE THAT!!!
            cellMatrix.initilizeFromOrigin();
            omp_set_num_threads(currThreadNum);
            outFile << endl << currThreadNum << ",";
            auto start = chrono::steady_clock::now();
            for(int i = 0; i < stepNum; i++){
                cellMatrix.fullStep();
            }
            auto end = chrono::steady_clock::now();
            outFile << chrono::duration_cast<chrono::milliseconds>(end - start).count() / (1000.0);
        }
    }
}

int diagonalNeighborProc(MPI_Comm comm, int right, int down, int cyclic, int processRow, int processCol){
    int rank, rank_dest, coords[2];
    MPI_Comm_rank(comm, &rank);
    MPI_Cart_coords(comm, rank, 2, coords);
    coords[0] += right;
    coords[1] += down;
    if((coords[0] < 0 or coords[1] < 0 or coords[0] >= processRow or coords[1] >= processCol) and (!cyclic)){
        return MPI_PROC_NULL;
    }
    MPI_Cart_rank(comm, coords, &rank_dest);
    return rank_dest;
}

map<string, int> allNeighborsProc(MPI_Comm comm, int cyclic, int processRow, int processCol){
    int left, right, top, bottom;
    MPI_Cart_shift(comm, 0, 1, &top, &bottom);
    MPI_Cart_shift(comm, 1, 1, &left, &right);
    map<string, int> neighbors {
        { "l", left },
        { "r", right },
        { "t", top },
        { "b", bottom },
        { "l-b", diagonalNeighborProc(comm, 1, -1, cyclic, processRow, processCol) },
        { "l-t", diagonalNeighborProc(comm, -1, -1, cyclic, processRow, processCol) },
        { "r-b", diagonalNeighborProc(comm, 1, 1, cyclic, processRow, processCol) },
        { "r-t", diagonalNeighborProc(comm, -1, 1, cyclic, processRow, processCol) }
    };
    return neighbors;
}

MPI_Comm initMpi(int argc, char *argv[], int periodic, int rows, int cols, int reorder){
    MPI_Comm comm;
    int dim[2], period[2];
    MPI_Init(&argc, &argv);
    dim[0] = rows; dim[1] = cols;
    period[0] = periodic; period[1] = periodic;
    MPI_Cart_create(MPI_COMM_WORLD, 2, dim, period, reorder, &comm);
    return comm;
}