import matplotlib.pyplot as plt
from os.path import exists
import os


DATA_FILE_PATH = r"times.txt"


def readDataFile(dataFile):
    rawData = dataFile.read()
    allData = rawData.strip().split('\n')
    runInfo = tuple(data for data in allData[0].split(', '))
    gridTimesInfo = [tuple([value for value in data.split(' ')]) for data in allData[1:]]
    plotTitle = allData[0]
    return plotTitle, gridTimesInfo, rawData


def totalSeconds(time):
    times = time.split(":")
    return float(times[0]) * 60 + float(times[1])


def findThreads(gridTimesInfo):
    threads = []
    for line in gridTimesInfo:
        thread = int(line[0])
        if thread not in threads:
            threads.append(thread)
    return threads


def plotDataFile(plotTitle, gridTimesInfo):
    threads = findThreads(gridTimesInfo)
    plt.title(plotTitle)
    for thread in threads:
        lengthEach = len(gridTimesInfo) // len(threads)
        startIndex = int(threads.index(thread) * lengthEach)
        label = str(thread) + " threads"
        plt.bar([int(data[2]) for data in gridTimesInfo[startIndex: startIndex + lengthEach]], [totalSeconds(data[-1]) for data in gridTimesInfo[startIndex: startIndex + lengthEach]], label = label)
    plt.xlabel("Matrix size")
    plt.ylabel("Runtime in seconds")
    plt.legend()
    plt.savefig(f"times/{plotTitle}/times.png")


def saveDataFile(plotTitle, data):
    filename = f"times/{plotTitle}/times.txt"
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, "w") as file:
        file.write(data)



def main():

    if not exists(DATA_FILE_PATH):
        print("\n### Data file is not ready yet or mislocated ###\n")
        exit(1)

    try:
        with open(DATA_FILE_PATH, 'r') as datafile:
            plotTitle, gridTimesInfo, rawData = readDataFile(datafile)
    except Exception:
        print("\n### An error has occurred while trying to read the file ###\n")
        exit(1)

    saveDataFile(plotTitle, rawData)
    plotDataFile(plotTitle, gridTimesInfo)


if __name__ == "__main__":
    main()