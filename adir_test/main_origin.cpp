#include <iostream>
#include <game_of_life.h>
#include <mpi.h>
#include <VisItControlInterface_V2.h>
#include <VisItDataInterface_V2.h>
#include "SimulationExample.h"
using namespace std;




int main(int argc, char *argv[]){

    FlagReader flagReader(0);
    flagReader.initialFlags(argc, argv);


    int nameLen, size, rank;
    char processorName[MPI_MAX_PROCESSOR_NAME];
    MPI_Comm comm = initMpi(argc, argv, flagReader.getCyclic(), flagReader.getProcessRow(), flagReader.getProcessCol(), flagReader.getProcessReorder());
    MPI_Get_processor_name(processorName, &nameLen);
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    
    
    int stepNum, width, length;
    if(rank == 0){
        if(!flagReader.getNoInput()){
            width = userInput("Please enter the width of the cells's matrix for each process: ", "The width of the matrix must be > zero!\n", 0);
            length = userInput("Please enter the length of the cells's matrix for each process: ", "The length of the matrix must be > zero!\n", 0);
            stepNum = userInput("Please enter the number of generations: ", "The number of generations must be >= zero!\n", -1);
        }
        else{
            width = flagReader.getMatrixRow(), length = flagReader.getMatrixCol(), stepNum = flagReader.getGenerations();
        }
    }

    MPI_Bcast(&width, 1, MPI_INT, 0, comm);
    MPI_Bcast(&length, 1, MPI_INT, 0, comm);
    MPI_Bcast(&stepNum, 1, MPI_INT, 0, comm);
    int maxStepNum = stepNum;

    CellMatrix cellMatrix(width, length, flagReader.getCyclic(), flagReader.getCommunication(), flagReader.getDemoCalc(), rank, size, nameLen, comm, flagReader.getProcessRow(), flagReader.getProcessCol(), flagReader.getThreads());
    cellMatrix.initilizeRandomMatrix(flagReader.getRandom());
    cellMatrix.setNeighbors(allNeighborsProc(comm, flagReader.getCyclic(), flagReader.getProcessRow(), flagReader.getProcessCol()));

    // plotData(40, flagReader.getPlot(), maxStepNum, cellMatrix);

    while(stepNum >= 0){
        writeMatrix(maxStepNum - stepNum, maxStepNum, flagReader.getFile(), flagReader.getTerminal(), cellMatrix, flagReader.getProcessRow(), flagReader.getProcessCol());
        cellMatrix.fullStep();
        stepNum--;
    }
    
    cellMatrix.freeMemory();
    
    MPI_Finalize();
    return 0;
}