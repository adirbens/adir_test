#include <gtest/gtest.h>
#include <game_of_life.h>



//   Non-cyclic test:
//   The matrix for the test will be:
//
//   1 0 1                                0 1 0                                 0 1 0
//   0 1 0     =>(after 1 generation)     1 0 1     =>(after 2 generations)     1 0 1
//   1 0 1                                0 1 0                                 0 1 0
//   
// We check here that the values after 2 generations are currect
TEST(nonCyclicFullStepTest, fullStep){
  MPI_Init(NULL, NULL);
  CellMatrix cellMatrix(3, 3, 0, 0, 0, 0, 1, 0, MPI_COMM_WORLD, 1, 1);
  cellMatrix.initilizeCostumeMatrix(2);

  cellMatrix.fullStep();
  EXPECT_EQ(cellMatrix.getCell(1, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 2), 1);
  EXPECT_EQ(cellMatrix.getCell(1, 3), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 1), 1);
  EXPECT_EQ(cellMatrix.getCell(2, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 3), 1);
  EXPECT_EQ(cellMatrix.getCell(3, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 2), 1);
  EXPECT_EQ(cellMatrix.getCell(3, 3), 0);

  cellMatrix.fullStep();
  EXPECT_EQ(cellMatrix.getCell(1, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 2), 1);
  EXPECT_EQ(cellMatrix.getCell(1, 3), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 1), 1);
  EXPECT_EQ(cellMatrix.getCell(2, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 3), 1);
  EXPECT_EQ(cellMatrix.getCell(3, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 2), 1);
  EXPECT_EQ(cellMatrix.getCell(3, 3), 0);

  cellMatrix.freeMemory();
}


//   Cyclic test:
//   The matrix for the test will be:
//
//   1 0 1                                0 0 0
//   0 1 0     =>(after 1 generation)     0 0 0
//   1 0 1                                0 0 0
//   
// We check here that the values after 1 generation are currect
TEST(CyclicFullStepTest, fullStep){
  CellMatrix cellMatrix(3, 3, 1, 0, 0, 0, 1, 0, MPI_COMM_WORLD, 1, 1);
  cellMatrix.initilizeCostumeMatrix(2);

  cellMatrix.fullStep();
  EXPECT_EQ(cellMatrix.getCell(1, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 3), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 3), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 3), 0);

  cellMatrix.freeMemory();
}


//   Cyclic test:
//   The matrix for the test will be:
//
//   1 1 0                                0 0 0
//   1 1 0     =>(after 1 generation)     0 0 0
//   1 1 0                                0 0 0
//   
// We check here that the values after 1 generation are currect
TEST(CyclicFullStepTest2, fullStep){
  CellMatrix cellMatrix(3, 3, 1, 0, 0, 0, 1, 0, MPI_COMM_WORLD, 1, 1);
  cellMatrix.initilizeCostumeMatrix(3);

  cellMatrix.fullStep();
  EXPECT_EQ(cellMatrix.getCell(1, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 3), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 3), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 3), 0);

  cellMatrix.freeMemory();
}


//   Cyclic test:
//   The matrix for the test will be:
//  
//   1 0 1 0                               1 0 1 0
//   1 0 1 0     =>(after 1 generation)    1 0 1 0
//   1 0 1 0                               1 0 1 0
//   1 0 1 0                               1 0 1 0
//                              
// We check here that the values after 1 generation are currect
TEST(CyclicFullStepTest3, fullStep){
  CellMatrix cellMatrix(4, 4, 1, 0, 0, 0, 1, 0, MPI_COMM_WORLD, 1, 1);
  cellMatrix.initilizeCostumeMatrix(2);

  cellMatrix.fullStep();
  EXPECT_EQ(cellMatrix.getCell(1, 1), 1);
  EXPECT_EQ(cellMatrix.getCell(1, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 3), 1);
  EXPECT_EQ(cellMatrix.getCell(1, 4), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 1), 1);
  EXPECT_EQ(cellMatrix.getCell(2, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 3), 1);
  EXPECT_EQ(cellMatrix.getCell(2, 4), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 1), 1);
  EXPECT_EQ(cellMatrix.getCell(3, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 3), 1);
  EXPECT_EQ(cellMatrix.getCell(3, 4), 0);
  EXPECT_EQ(cellMatrix.getCell(4, 1), 1);
  EXPECT_EQ(cellMatrix.getCell(4, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(4, 3), 1);
  EXPECT_EQ(cellMatrix.getCell(4, 4), 0);

  cellMatrix.freeMemory();
}


//   Cyclic test:
//   The matrix for the test will be:
//  
//   1 0 1 0 1                              0 0 1 0 0
//   1 0 1 0 1    =>(after 1 generation)    0 0 0 0 0
//   1 0 1 0 1                              0 0 1 0 0
//                              
// We check here that the values after 1 generation are currect
TEST(CyclicFullStepTest4, fullStep){
  CellMatrix cellMatrix(3, 5, 1, 0, 0, 0, 1, 0, MPI_COMM_WORLD, 1, 1);
  cellMatrix.initilizeCostumeMatrix(2);

  cellMatrix.fullStep();
  EXPECT_EQ(cellMatrix.getCell(1, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 3), 1);
  EXPECT_EQ(cellMatrix.getCell(1, 4), 0);
  EXPECT_EQ(cellMatrix.getCell(1, 5), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 3), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 4), 0);
  EXPECT_EQ(cellMatrix.getCell(2, 5), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 1), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 2), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 3), 1);
  EXPECT_EQ(cellMatrix.getCell(3, 4), 0);
  EXPECT_EQ(cellMatrix.getCell(3, 5), 0);

  cellMatrix.freeMemory();
}


//   Non-cyclic test:
//   The matrix for the test will be:
//
//   1 0 1
//   0 1 0
//   1 0 1                 
//   
// We check here only the "out of bound" cells
TEST(nonCyclicNeighborTest, isNeighbor){
  CellMatrix cellMatrix(3, 3, 0, 0, 0, 0, 1, 0, MPI_COMM_WORLD, 1, 1);
  cellMatrix.initilizeCostumeMatrix(2);

  EXPECT_EQ(cellMatrix.isNeighbor(0, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(1, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(2, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(3, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(4, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(0, 4), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(1, 4), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(2, 4), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(3, 4), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(4, 4), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(0, 1), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(0, 2), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(0, 3), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(4, 1), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(4, 2), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(4, 3), 0);

  cellMatrix.freeMemory();
}


//   Cyclic test:
//   The matrix for the test will be:
//  
//                                  0 1 0 1 0 1
//   1 0 1 0                        0 1 0 1 0 1
//   1 0 1 0     =>(with cyclic)    0 1 0 1 0 1
//   1 0 1 0                        0 1 0 1 0 1
//   1 0 1 0                        0 1 0 1 0 1
//                                  0 1 0 1 0 1
//   
// We check here only the "out of bound" cells
TEST(cyclicNeighborTest, isNeighbor){
  CellMatrix cellMatrix(4, 4, 1, 0, 0, 0, 1, 0, MPI_COMM_WORLD, 1, 1);
  cellMatrix.initilizeCostumeMatrix(2);

  EXPECT_EQ(cellMatrix.isNeighbor(0, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(1, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(2, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(3, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(4, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(5, 0), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(0, 5), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(1, 5), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(2, 5), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(3, 5), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(4, 5), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(5, 5), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(0, 1), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(0, 2), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(0, 3), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(0, 4), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(5, 1), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(5, 2), 0);
  EXPECT_EQ(cellMatrix.isNeighbor(5, 3), 1);
  EXPECT_EQ(cellMatrix.isNeighbor(5, 4), 0);

  cellMatrix.freeMemory();
  MPI_Finalize();
}