#!/bin/bash


#
# configurations
#
let processRow=32
let processCol=4
let processNum=$processRow*$processCol
let genNum=50
let threadNum=1
let matrixSize=4800
let isCyclic=1

let gatherTimes=0
let calcLoadBalance=0
let calcAvgMaxRatio=0

if [ -d "communicationTimes" ]; then
  rm -f communicationTimes/*.txt
fi


#
# 
#
if [ $isCyclic -eq 0 ]; then
  totalTime=$( { \time -f "%e" mpirun -n $processNum ./build/game_of_life row=$processRow col=$processCol matrixr=$matrixSize matrixc=$matrixSize gen=$genNum threads=$threadNum -n -m; } 2>&1 )
else
  totalTime=$( { \time -f "%e" mpirun -n $processNum ./build/game_of_life row=$processRow col=$processCol matrixr=$matrixSize matrixc=$matrixSize gen=$genNum threads=$threadNum -n -m -c; } 2>&1 )
fi

if [ $gatherTimes -eq 0 ]; then
  if [ $calcLoadBalance -eq 1 -o $calcAvgMaxRatio -eq 1 ]; then
    echo "Must gather times before making any computations! -_-"
    exit 1
  fi
fi
if [ $gatherTimes -eq 1 ]; then
  python -c"import game_of_life_communication; game_of_life_communication.writeTimes(${processNum})"
fi

if [ $calcLoadBalance -eq 1 ]; then
  python -c"import game_of_life_communication; game_of_life_communication.plotLoadBalance(${processNum}, ${isCyclic}, ${totalTime}, ${matrixSize}, ${threadNum})"
fi

if [ $calcAvgMaxRatio -eq 1 ]; then
  python -c"import game_of_life_communication; game_of_life_communication.calcAvgMaxRatio(${processNum})"
fi




# no bindings - 1 thread - 129.56s
#\time -f "%e" mpirun -n $processNum -host rfwwwlhpchim1:-1,rfwwwlhpchim2:-1,rfwwwlhpchim3:-1#,rfwwwlhpchim4:-1 ./build/game_of_life row=$processRow col=$processCol matrixr=$matrixSize matrixc=#$matrixSize gen=$genNum threads=$threadNum -n -m

# 1 core 1 process - 1 thread - 129.62s
#\time -f "%e" mpirun -n $processNum --map-by numa:PE=1 -host rfwwwlhpchim1:-1,rfwwwlhpchim2:-1#,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build/game_of_life row=$processRow col=$processCol matrixr=#$matrixSize matrixc=$matrixSize gen=$genNum threads=$threadNum -n -m

# no bindings - 1 thread - 201.36s
#\time -f "%e" mpirun -n $processNum -host rfwwwlhpchim1:-1,rfwwwlhpchim2:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build/game_of_life row=#$processRow col=$processCol matrixr=6000 matrixc=6000 gen=$genNum threads=$threadNum reorder=1 -n -ma

# 1 core 1 process - 1 thread - 201.76s
#\time -f "%e" mpirun -n $processNum --map-by numa:PE=1 -host rfwwwlhpchim1:-1,rfwwwlhpchim2:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build#/game_of_life row=$processRow col=$processCol matrixr=6000 matrixc=6000 gen=$genNum threads=$threadNum reorder=1 -n -m

# no bindings - 2 threads - 140.86s
#\time -f "%e" mpirun -n $processNum -host rfwwwlhpchim1:-1,rfwwwlhpchim2:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build/game_of_life row=#$processRow col=$processCol matrixr=$matrixSize matrixc=$matrixSize gen=$genNum threads=2 -n -m

# 1 core 1 process - 2 threads - 130.20s
#\time -f "%e" mpirun -n $processNum --map-by numa:PE=1 -host rfwwwlhpchim1:-1,rfwwwlhpchim2:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build#/game_of_life row=$processRow col=$processCol matrixr=$matrixSize matrixc=$matrixSize gen=$genNum threads=2 -n -m

#16 core 1 process (process per numa node) - 16 threads per process - weak scalability - 9.41s
#\time -f "%e" mpirun -n 8 --bind-to core --map-by ppr:1:numa:PE=16 -host rfwwwlhpchim1:-1,rfwwwlhpchim2#:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build/game_of_life row=4 col=2 matrixr=$matrixSize matrixc=#$matrixSize gen=$genNum threads=16 -n -m

#16 core 1 process (process per numa node) - 32 threads per process - weak scalability - 10.07s
#\time -f "%e" mpirun -n 8 --bind-to core --map-by ppr:1:numa:PE=16 -host rfwwwlhpchim1:-1,rfwwwlhpchim2#:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build/game_of_life row=4 col=2 matrixr=$matrixSize matrixc=#$matrixSize gen=$genNum threads=32 -n -m

#16 core 1 process (process per numa node) - 4 threads per process - strong scalability - 526.36s
#\time -f "%e" mpirun -n 8 --bind-to core --map-by ppr:1:numa:PE=16 -host rfwwwlhpchim1:-1,rfwwwlhpchim2:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:#-1 ./build/game_of_life row=4 col=2 matrixr=19200 matrixc=19200 gen=$genNum threads=4 -n -m

#16 core 1 process (process per numa node) - 8 threads per process - strong scalability - 269.56s
#\time -f "%e" mpirun -n 8 --bind-to core --map-by ppr:1:numa:PE=16 -host rfwwwlhpchim1:-1,rfwwwlhpchim2:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:#-1 ./build/game_of_life row=4 col=2 matrixr=19200 matrixc=19200 gen=$genNum threads=8 -n -m

#16 core 1 process (process per numa node) - 16 threads per process - strong scalability - 141.28s
#\time -f "%e" mpirun -n 8 --bind-to core --map-by ppr:1:numa:PE=16 -host rfwwwlhpchim1:-1,rfwwwlhpchim2#:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build/game_of_life row=4 col=2 matrixr=19200 matrixc=19200 gen=#$genNum threads=16 -n -m

#16 core 1 process (process per numa node) - 25 threads per process - strong scalability - 153.03s
#\time -f "%e" mpirun -n 8 --bind-to core --map-by ppr:1:numa:PE=16 -host rfwwwlhpchim1:-1,rfwwwlhpchim2#:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build/game_of_life row=4 col=2 matrixr=19200 matrixc=19200 gen=#$genNum threads=25 -n -m

#16 core 1 process (process per numa node) - 32 threads per process - strong scalability - 148.09s
#\time -f "%e" mpirun -n 8 --bind-to core --map-by ppr:1:numa:PE=16 -host rfwwwlhpchim1:-1,rfwwwlhpchim2#:-1,rfwwwlhpchim3:-1,rfwwwlhpchim4:-1 ./build/game_of_life row=4 col=2 matrixr=19200 matrixc=19200 gen=#$genNum threads=32 -n -m



# KILL ZOMBIES
#
# ps aux | grep game_of | cut -d ' ' -f 2 | xargs -n 1 kill -KILL
#
# ps aux | grep game_of
#
#
#
# :::DEFAULT BINDINGS:::
#
# MCW rank 0 bound to: [B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B][./././././././././././././././.]
# MCW rank 1 bound to: [./././././././././././././././.][B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B]
# MCW rank 2 bound to: [B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B][./././././././././././././././.]
# .
# .
# .
# MCW rank 11 bound to: [./././././././././././././././.][B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B]
# .
# .



# 4087.25user 11.42system 2:08.32elapsed 3194%CPU (0avgtext+0avgdata 193976maxresident)k
# 0inputs+17,672outputs (3,127major+10,078,437minor)pagefaults 0swaps

# 4612.93user 12.24system 2:24.85elapsed 3192%CPU (0avgtext+0avgdata 193908maxresident)k
# 0inputs+21,512outputs (3,125major+10,192,760minor)pagefaults 0swaps


# mpirun -n 32 --map-by core:PE=1 /home/adirbens/intel/oneapi/vtune/2023.0.0/bin64/vtune -collect hotspots -k sampling-mode=sw -trace-mpi -no-follow-child --result-dir /home/adirbens/test/adir_test/vtune-examples/t1 /home/adirbens/test/adir_test/build/game_of_life row=8 col=4 -n matrixr=2400 matrixc=2400 gen=100




